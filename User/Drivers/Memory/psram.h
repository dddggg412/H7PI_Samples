#ifndef __PSRAM_H__
#define __PSRAM_H__


/* ����ͷ�ļ� ----------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

#ifdef __cplusplus
extern "C" {
#endif
	
extern  SPI_HandleTypeDef hspi2;
#define phspi hspi2

#define LY68L_CS_Pin GPIO_PIN_13
#define LY68L_CS_GPIO_Port GPIOC

#define LY68L_Read              0x03
#define LY68L_FastRead          0x0B
#define LY68L_FastReadQuad      0xEB
#define LY68L_Write             0X02
#define LY68L_QuadWrite         0X38
#define LY68L_EnterQuadMode     0X35
#define LY68L_ExitQuadMode      0XF5
#define LY68L_ResetEnable    		0X66
#define LY68L_Reset         		0X99
#define LY68L_SetBurstLength		0XC0
#define LY68L_ReadID        		0X9F

#define LY68L_MFID              0X0D
#define LY68L_KGD_Fail          0X55
#define LY68L_KGD_Pass          0X5D

uint8_t  LY68L_SPI_Init(void);
uint32_t LY68L_SPI_ReadID(void);
uint8_t  LY68L_SPI_Init(void);
uint8_t  LY68L_SPI_Read(uint8_t* pBuffer,uint32_t ReadAddr,uint16_t Length);
uint8_t  LY68L_SPI_FastRead(uint8_t* pBuffer,uint32_t ReadAddr,uint16_t Length);
uint8_t  LY68L_SPI_Write(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t Length);
uint8_t  LY68L_SPI_Test(void);
void     LY68L_SPI_Reset(void);

#ifdef __cplusplus

}
#endif
#endif


